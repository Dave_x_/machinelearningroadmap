# MachineLearningRoadMap

"To all those who were following my previous post on LinkedIn, I've decided to create a roadmap. I hope this is as useful as it is intended to track your progress on those important topics for starting a career in ML. Hopefully, this will work for you. I must admit that there were so many sources of information to get this done, but this work is primarily based on a diploma offered by Sergio Arboleda University. You can find the link here: https://www.usergioarboleda.edu.co/educacion-continuada/curso-en-inteligencia-artificial/"
